#   A function that generates a clique of edges when given a dictionary of authors for a paper.
# Returns a list of tuples representing the start and end of each edge. The tuple will be sorted smallest id first
# simplify database queries.

from database_operations import get_author_id

def edge_generator(authors, cursor):
    edges = []
    author_ids = []

    # Get author IDs from authors table
    for author in authors:
        author_ids.append(get_author_id(author, cursor))
    author_ids.sort()

    # This is Python as fuck:
    for author1 in author_ids:
        for author2 in author_ids:
            if author1 < author2:
                edges.append((author1,author2))

    return edges
