#!/usr/bin/python

import sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')
import gv

from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.minmax import *
from pygraph.readwrite.dot import write

import MySQLdb as mdb
from database_operations import *

class collaborationGraph:

    def __init__(self, db, weighted=0, draw=0):
        self.collaboration_graph = graph()

        con, cursor = connectdb(db)

        # Get number of edges
        cursor.execute("SELECT MAX(id) FROM authors")
        self.num_authors = cursor.fetchall()[0][0]
        print "Number of authors:", self.num_authors

        # Only care about numerical id for nodes
        self.collaboration_graph.add_nodes(range(1, self.num_authors+1))

        cursor.execute("SELECT MAX(id) FROM edges")
        num_edges = cursor.fetchall()[0][0]
        print "Number of edges:", num_edges

        # Query all for edge in range(1,num_edges):
        cursor.execute("SELECT * FROM edges")

        # Gotta fetch 'em all
        while True:
            edge = cursor.fetchone()
            if not edge:
                break
            if weighted:
                self.collaboration_graph.add_edge((int(edge[1]),int(edge[2])), 1/float(edge[3]))
            else:
                self.collaboration_graph.add_edge((int(edge[1]),int(edge[2])))

        if draw:
            dot = write(self.collaboration_graph)
            gvv = gv.readstring(dot)
            gv.layout(gvv,'neato')
            gv.render(gvv,'png',db+'-neato.png')
    

    def get_shortest_paths(self, start_node):
        return shortest_path(self.collaboration_graph, start_node)[1]


    def get_adjacency_matrix(self):
        return [[int(self.collaboration_graph.has_edge((start,end))) for end in range(1,self.num_authors+1)] for start in range(1,self.num_authors+1)]
