\title{ Link Prediction for Plagiarism Detection and Name Disambiguation in Collaboration Networks }
\author { Rudi Plesch \\ rplesch@ece.ubc.ca}
\date{\today}

\documentclass{article}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}

\begin{document}
\maketitle

\begin{abstract}
The authors propose a novel method for predicting collaboration in online collaboration databases. 
Using both weighted and unweighted representations of existing collaborations to generate a new set of features for machine learning, prediction accuracy was above 95\%.
This method has important implications for modelling recurrence of interactions in social networks, and can be used to detect plaigirism and disambiguate names in collaboration networks.
\end{abstract}

\section{Introduction and Previous Work}
Online scientific publication databases store publications, which are often a collaboration of many authors.
These networks can be modelled as a graph with nodes representing authors and edges between them that represent collaborations.
In this context, the link prediction problem attemps to predict new interactions in this graph.

Work in link prediction traditionally focuses on predicting links emerging between entities in a network that have not previously interacted.
This is equivalent to predicting emergence of new edges in an undirected, unweighted graph.
See\cite{Hasan06linkprediction,Liben-nowell_thelink-prediction}.
This method is not well suited to the problem of prediciting when authors will collaborate, since not only new collaborations are intersting, but also recurring ones.

As in traditional link prediction, this problem can be viewed as a two-class categorization problem with potential edges classified into likely to emerge or not.
However to adapt this fomulation to predicting recurrence of existing links, we used features from both an unweighted graph of the collaborations, as in \cite{hassansurvey} and \cite{Hasan06linkprediction}, and an unweighted graph that encoded how many times a pair of authors have collaborated.

There are many comprehensive literature surveys of traditional link prediction, including \cite{hassansurvey} and \cite{anothersurvey}, but, to the best of the authors' knowledge, there has not been another similar treatment of link prediction.

\section{Methodology}
For the dataset used in these experiments is the snapshot of the high energy physics database maintained by arXiv.com.
The dataset is available from Cornell Univeristy.
After removing partial names and assuming that all authors with the same initials and surname are the same person, the database contained 9066 authors.
The authors formed 17390 collaborating pairs in 26098 publications.
The publications with one author were not considered.

\subsection{Feature Extraction}
We constructed two graphs that represented the collaboration network.
First, an unweighted graph with an edge representing any number of collaborations in the training interval, and, secondly, a weighted graph.
Inuitively, authors that have collaborated frequently are more likely to continue collaborating, therefore the cost of an edge should decrease with more collaborations.
We modelled this as analogous to resistors in an electrical circuit, each new collaboration would add a new parallel edge between the nodes and the edge weight in the weighted graph, or the resistance, would be the inverse of the number of edges.
Examples in link predictions literature \cite{Hasan06linkprediction,anothersurvey,hassansurvey} describe many features that can be used to predict links, but were able to produce very good predictions with a small set of computationally inexpensive neighborhood and path features.
The features were all calculated for pairs of nodes.
\subsubsection{Neighborhood features}
The neighborhood features of the nodes are the same in the weighted and unweighted graphs.
In the descriptions below, $\Gamma(n)$ denoted the neigbour set of node $n$.
\begin{description}
    \item[Number of common neighbors] For nodes $n$ and $m$:
        \begin{equation*}
        |\Gamma(n) \cap \Gamma(m)|
        \end{equation*}
    \item[Jaccard similarity of neighbors] For nodes $n$ and $m$:
        \begin{equation*}
        \frac{|\Gamma(n) \cap \Gamma(m)|}{|\Gamma(n) \cup \Gamma(m)|}
        \end{equation*}
    \item[Number of common neighbors of neighbors] For nodes $n$ and $m$:
        \begin{equation*}
        \left| \bigcup_{x \in \Gamma(n)}\Gamma(x) \ \cap \bigcup_{y \in \Gamma(m)}\Gamma(y)\right|
        \end{equation*}
    \item[Adamic/Adar] This is similar to common neighborhood, but weights common neighbors with a smaller degree more highly.
        For nodes $n$ and $m$:
        \begin{equation*}
        \sum_{z \in \Gamma(m) \cap \Gamma(n)} \frac{1}{\log|\Gamma(z)|}
        \end{equation*}
\end{description}
\subsubsection{Path features}
It is common in link prediction literature to use unweighted shortest path between a set of nodes as a feature, with the intuition that if two nodes are separated by a small number of hops, they are more likely to interact.
It is a novel contribution of this paper, however, to represent frequentely interacting nodes with a small edge weight and use a weighted shortest path as a feature.
In both cases, shortest paths were calculated between all pairs of nodes.
If no path existed between two nodes, we used 1,000,000,000 as the shortest path cost, which is much larger than any path in the graph.
We opted for this in order to make all the features comparable accross all pairs of nodes.

Katz centrality\cite{katz} is also commonly used in link prediction\cite{Hasan06linkprediction,anothersurvey,hassansurvey}.
It accounts for all paths between two nodes while penalizing long paths with a coefficient $\beta$ raised to the power of the length of the path it is generally expressed as:
\begin{equation*}
\sum\limits_{l=1}^{\infty}\beta^l\#(\text{paths with length $l$ between nodes})
\end{equation*}
It can also be found using the adjacency matrix $A$ of a graph by $(I-\beta A)^{-1} - I$.
We used $\beta$ of 0.01, which was small enough to make the matrix invertible.

\subsection{Statistical Analysis}
The prediction of new publications is a classification problem with two categories, an edge emerging in the test set or not.
In a typical machine learning classification problem, an algorithm can be trained on a the features of node pairs in the test set and evaluated on features of node pairs in the test set. 
This method requires examples of both categories, meaning node pairs with an edge existing between them and not.
One of the large problems that other researchers have encounterd is a large class skew\cite{hassansurvey}.
Most collaboration networks, including the one studied, are quite sparse.
The arXiv high energy physics database graph has 17390 edges for 9066 nodes.
This situation can be problematic for some classifiers, as the distribution of the larger class can completely mask the distribution of the smaller class in the covariate space.
A standard approach in statistics for dealing with this problem is to sample the more frequently occurring class, including fewer examples in the machine learning dataset\cite{rarity}.
Each fold contained around 11000 non-existing edges and 3000 existing edges.
When generating the testing and training sets, only 1/500 pairs of nodes with no edge between them were randomly selected for inclusion.
We also repeated the test with 1/100 random sampling, and the results did not change.

We tested several machine learning algorithms, including parametric and non-parametric models.
Simple parametric models like Linear Discriminant Analysis and General Linear Models for classifications are not appropriate in this application because the features are highly colinear.
This makes the matrices that the matrices that these algorithms need to invert are close to singluar, making results unreliable.
Stochastic Gradient Decent (SGD) was used instead of these models.
Non-parametric models tested are, Support Vector Machines (SVM), Random Forests, boosted stumps, and K-Nearest-Neighbors (KNN).

To evaluate the preformance of classifiers, we used ten-fold cross-validation.
The data, both existing edges and downsampled non-existing edges, were randomly separated into ten sets.
The parallel edges in that represented multiple publications between authors were treaded individually in this random selection, meaning that the algorithm predicted both new collaborations and repated collaborations. 
For each fold of the cross-validation was used as a test set once.
We constructed weighted and unweighted graphs and using nine training folds and extracted the features for node pairs in the test and trainig sets from these graphs.
We then trained the algorithms for on the nine test fold and predicted the left-out fold.

We performed all analysis with the numpy and scikit-learn packages in Python\cite{scikit-learn}

% Stochastic gradient descrent once had 25% error, convergence problem
\section{Results}
All machine learning algorithms showed good prediction power, however, KNN was by far the best model.
The results of the best classifiers in each category are summarized in table \ref{table:results}.
Below, we dicuss in detail the performance of each classifier type.

\begin{description}
    \item[Tree-based classifiers:] These were the worst-performing classifiers of the group.
        Changing the number of trees and the depth of trees used did not dramatically improve performance. 
        One notable observation is that in every cross-validation fold, the error of boosted trees and random forests was the same.
        They likely made errors on the same predictions.
    \item[SVM:] These classifiers showed consistently mediocre performance in all tests.
    \item[SGD:] This algorithm showed very good classification error rate in many cross validation tests, reaching as low as 4\%.
        It showed a convergence problem in a few situations that led to 25\% error rate.
    \item[KNN:] This was by far the best classifier, using a single nearest neighbor produced a classification error rate of 4\%.
        Increasing the number of neighbor lowered the classification error rate until to plateaued at 50 neighbors.
\end{description}

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Classifier} & \textbf{SVM} & \textbf{SGD} & \textbf{Random Forest} & \textbf{Boosted Stumps} & \textbf{KNN 10} & \textbf{KNN 50} \\
\hline
\textbf{CER} & 0.09289 & 0.07244 & 0.1031 &  0.1031 & 0.04927 & \textbf{0.02975}\\
\hline
\end{tabular}
\caption{Average classificaton error rate (CER) over several runs of cross-validation for all classifiers}
\label{table:results}
\end{table}

\subsection{Feature Distribution}
Analysis of the distribution of features shows some interesting conclusions.
As shown in figure \ref{fig:adamicnegihbors}, the Adamic/Adar and common neighbor values of paris of nodes are highly correlated as is the same for the common secondary neighbors and Jaccard similarity of neighbors.
This makes them essentially interchangable, and removing any of them would likely have left some classifiers, like KNN and random forests, unaffected and would have slightly improved the performance of the rest.
The distribution also shows that any nodes with common neighbors are very likely to collaborate in the test interval.
This is likely a property of the collaboration network.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{adamic_common.png}
\caption{Distribution of Adamic/Adar and common neighbors for examples of non-coauthoring pairs (blue) and coauthoring pairs (red)}
\label{fig:adamicnegihbors}
\end{figure}

Predictably, unweighted shortest path and weighted weighted shortest path are correlated, but there is a clear trend that authors separated by the same number of hops are more likely to collaborate if they have a path with more collaborations joining them, the distribution is shown in figure \ref{fig:weightunweight}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{weight_unweight.png}
\caption{Distribution of weighted and unweighted shortest path for examples of non-coauthoring pairs (blue) and coauthoring pairs (red)}
\label{fig:weightunweight}
\end{figure}



% Future work: determining which features are important, new, better features (keywords, related fields)
\section{Conclusion and Future Work}

\bibliographystyle{plain}
\bibliography{hassan,linksurvey,kleinberg,anothersurvey,katz,rarity,scikit}
\end{document}

