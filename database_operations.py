#	Generally useful functions for the database

import MySQLdb as mdb
from pprint import pprint
from pygraph.classes.graph import graph
from pygraph.algorithms.minmax import shortest_path

def get_author_id(author, cursor):
    author_query = "SELECT * FROM authors WHERE name = '{name}'"
    
    cursor.execute(author_query.format(name=author))
    id_row = cursor.fetchall()

    return id_row[0][0]


def get_authorname(author_id, cursor):
    author_query = "SELECT * FROM authors WHERE id = {auth_id}"
    
    cursor.execute(author_query.format(auth_id=author_id))
    row = cursor.fetchall()

    return row[0][1]


# Taked an author and returns a list of tuples, each one is the edge ID, neigbour ID, and the edge weight
def get_neighbours(author_id, cursor):
    author_query = "SELECT * FROM edges WHERE start_node={id} OR end_node={id}"

    cursor.execute(author_query.format(id=author_id))
    rows = cursor.fetchall()

    neighbours = []
    for row in rows:
        if row[1] == author_id:
            neighbours.append((row[0],row[2],row[3]))
        else:
            neighbours.append((row[0],row[1],row[3]))
            
    return neighbours 
    

# Open a database connection
def connectdb(database):
    try:
        con = mdb.connect('localhost', 'rudi', 'password', database)

        cursor = con.cursor()
        cursor.execute("SELECT VERSION()")

        version = cursor.fetchone()

        print "Database version %s " %version

    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0],e.args[1])

    return con, cursor

# Close database connection
def disconnectdb(con):
    con.commit()
    con.close()

