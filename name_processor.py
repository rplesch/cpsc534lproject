#   A function that takes the author names in a line from our file and returns a list of author names in this format:
# Initials and last name if given names have no hypens
# Initial, period, hyphen, initial, period for a hypenated given name
# We're not changing surnames.

import re

def author_processor(authors):
    # This isn't perfect, but I'm sick of it
    name_checker = re.compile('([A-Z-].)+[A-Za-z]{2,}$')
    processed_authors = []
    # Split each author's name to only keep initials and last name
    for auth in authors:
        names = auth.split(' ')
        for n in range(len(names)-1):
            # Hyphenate initials properly
            if '-' in names[n] and len(names[n])>3:
                firstname = names[n].split('-')
                for i in range(2):
                    firstname[i] = firstname[i][0]+'.'
                names[n]=''.join([firstname[0], '-', firstname[1]])
            # Just take initial
            else:
                names[n] = names[n][0]+'.'
        auth = ''.join(names)
        # Now to deal with this kind of crap: Vl.S.Dotsenko, also a last check for malformed names
        if re.match(name_checker, auth):
            names = auth.split('.')
            for i in range(len(names)-1):
                if names[i][0] is not '-':
                    names[i] = names[i][0]
                names[i] = names[i]+'.'
            auth = ''.join(names)

            processed_authors.append(auth)

    return processed_authors

