\title{ Link Prediction and Textual Analysis for Name Disambiguation and Plagiarism Detection in Collaboration Networks }
\author { Rudi Plesch \\ University of British Columbia \\ rplesch@ece.ubc.ca
		  \and
		  Hootan Rashtian \\ University of British Columbia \\ rhootan@ece.ubc.ca}
		  
\date{\today}

\documentclass{article}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}
\usepackage{setspace}
\usepackage{url}
%\usepackage{hyperref}

\begin{document}
\maketitle
\doublespacing
\begin{abstract}

Name ambiguity and plagiarism are known as important problems in the context of publication networks and digital academic libraries. In this project, we propose a novel method for predicting collaboration in online collaboration databases. 
Using both weighted and unweighted representations of existing collaborations to generate a new set of features for machine learning, prediction accuracy  of collaborations was above 95\%. 
Moreover, we employed unsupervised learning in order to cluster authors with similar writing styles as well as identifying the writing style of paper with multiple authors.
We could develop clusters in which there was a dominant author was involved with up to 34\% of assigned papers in that cluster. 
These methods have important implications for modelling recurrence of interactions in social networks, and can be used to detect plagiarism and disambiguate names in collaboration networks.

\end{abstract}

\section{Introduction and Previous Work}
Usage of online digital libraries (DLs) has grown rapidly both in popularity and quantity as it provides users with variety of valuable features.
For instance, in the academic world, there are DLs such as ResearchGate \cite{researchgate}, DBLP \cite{dblp}, and CiteSeer \cite{citeseer}, that provide different features to facilitate literature discovery and services.
On the other hand, it has been previously reported that there are different challenges including ``ambiguous author names'' to have high quality content and services on DLs \cite{Lee:07}.
Some examples of name abiguity are at least one of the following takes place in a set of papers:
\begin{itemize}
\item There are unique authors with similar names (polysems \cite{polysemy}).
\item Authors sometimes using full names and other times initials.
\item There are authors with similar initial for first names while having similar surnames.
\end{itemize}
This could happen for reasons such as automatic harvesting \cite{Lagoze:2001} and existence of multiple standards for storing information and publication formats.

The emergence of online DLs can give a snapshot of the state of research and collaborations in an entire field, allowing the development of new, powerful plagiarism detection alogrithms.

We investigated potential solutions to these problems. For this aim, we tried to tailor ``link prediction'' and ``clustering''.
As previously mentioned, online DLs or scientific publication databases store publications, which are often a collaboration of many authors.
These networks can be modelled as a graph with nodes representing authors and edges between them that represent collaborations.
In this context, the link prediction problem attempts to predict new interactions in this graph. Work in link prediction traditionally focuses on predicting links emerging between entities in a network that have not previously interacted.
This is equivalent to predicting emergence of new edges in an undirected, unweighted graph.
See\cite{Hasan06linkprediction,Liben-nowell_thelink-prediction}.
However, this method is not well suited to the problem of predicting when authors will collaborate, since not only new collaborations are interesting, but also recurring ones.

As in traditional link prediction, this problem can be viewed as a two-class categorization problem with potential edges classified into likely to emerge or not.
However to adapt this formulation to predicting recurrence of existing links, we used features from both an unweighted graph of the collaborations, as in \cite{hassansurvey} and \cite{Hasan06linkprediction}, and an unweighted graph that encoded how many times a pair of authors have collaborated.

There are many comprehensive literature surveys of traditional link prediction, including \cite{hassansurvey} and \cite{anothersurvey}, but, to the best of the our knowledge, there has not been another similar treatment of link prediction.

We also considered unsupervised learning to investigate possibility of answering to our questions.
This approach is traditionally called as authorship attribution and authorship identification \cite{stamatatos:2009}.
However, a notable part of the work done in this area is limited to the scenario in which each document has a single author.
For this problem, researchers have applied supervised learning techniques and proposed classification algorithms to classify documents with unknown authors.
These methods have been applied authorship identification for other types of documents such as email, short text, and blogs \cite{de2001:mining, stamatatos:2009}.
There is also work on decomposition of multi-author documents in which segmentation algorithms are used followed by clustering algorithms to identify authors of different parts a document \cite{akiva:2013}.
Our approach is different with previous work because it is not based on the assumption that a paper is written by all of the authors or only by one author.
Rather, we assume that the text of the paper was written by any subset of the claimed authors.
We chose to follow unsupervised learning as our dataset was more fitted to this approach. We did not have labeled data such that we can use classification technique. However, it does not mean it is impossible to choose supervised learning here.
Having our assumption was aligned with the approach because this way, results of clustering could serve for solving more problems as discussed in \ref{clustering}. 

\section{Link Prediction Methodology} 
The dataset used in these experiments is the snapshot of the high energy physics database maintained by arXiv.com.
The dataset is available from Cornell University.
After removing partial names and assuming that all authors with the same initials and surname are the same person, the database contained 9066 authors.
The authors formed 17390 collaborating pairs in 26098 publications.
The publications with one author were not considered.

\subsection{Feature Extraction}
We constructed two graphs that represented the collaboration network.
First, an unweighted graph with an edge representing any number of collaborations in the training interval, and, secondly, a weighted graph.
Intuitively, authors that have collaborated frequently are more likely to continue collaborating, therefore the cost of an edge should decrease with more collaborations.
We modelled this as analogous to resistors in an electrical circuit, each new collaboration would add a new parallel edge between the nodes and the edge weight in the weighted graph, or the resistance, would be the inverse of the number of edges.
Examples in link predictions literature \cite{Hasan06linkprediction,anothersurvey,hassansurvey} describe many features that can be used to predict links, but were able to produce very good predictions with a small set of computationally inexpensive neighborhood and path features.
The features were all calculated for pairs of nodes.
\subsubsection{Neighborhood features}
The neighborhood features of the nodes are the same in the weighted and unweighted graphs.
In the descriptions below, $\Gamma(n)$ denoted the neighbor set of node $n$.
\begin{description}
    \item[Number of common neighbors] For nodes $n$ and $m$:
        \begin{equation*}
        |\Gamma(n) \cap \Gamma(m)|
        \end{equation*}
    \item[Jaccard similarity of neighbors] For nodes $n$ and $m$:
        \begin{equation*}
        \frac{|\Gamma(n) \cap \Gamma(m)|}{|\Gamma(n) \cup \Gamma(m)|}
        \end{equation*}
    \item[Number of common neighbors of neighbors] For nodes $n$ and $m$:
        \begin{equation*}
        \left| \bigcup_{x \in \Gamma(n)}\Gamma(x) \ \cap \bigcup_{y \in \Gamma(m)}\Gamma(y)\right|
        \end{equation*}
    \item[Adamic/Adar] This is similar to common neighborhood, but weights common neighbors with a smaller degree more highly.
        For nodes $n$ and $m$:
        \begin{equation*}
        \sum_{z \in \Gamma(m) \cap \Gamma(n)} \frac{1}{\log|\Gamma(z)|}
        \end{equation*}
\end{description}
\subsubsection{Path features}
It is common in link prediction literature to use unweighted shortest path between a set of nodes as a feature, with the intuition that if two nodes are separated by a small number of hops, they are more likely to interact.
It is a novel contribution of this paper, however, to represent frequently interacting nodes with a small edge weight and use a weighted shortest path as a feature.
In both cases, shortest paths were calculated between all pairs of nodes.
If no path existed between two nodes, we used 1,000,000,000 as the shortest path cost, which is much larger than any path in the graph.
We opted for this in order to make all the features comparable across all pairs of nodes.

Katz centrality\cite{katz} is also commonly used in link prediction\cite{Hasan06linkprediction,anothersurvey,hassansurvey}.
It accounts for all paths between two nodes while penalizing long paths with a coefficient $\beta$ raised to the power of the length of the path it is generally expressed as:
\begin{equation*}
\sum\limits_{l=1}^{\infty}\beta^l\#(\text{paths with length $l$ between nodes})
\end{equation*}
It can also be found using the adjacency matrix $A$ of a graph by $(I-\beta A)^{-1} - I$.
We used $\beta$ of 0.01, which was small enough to make the matrix invertible.

\subsection{Statistical Analysis}
The prediction of new publications is a classification problem with two categories, an edge emerging in the test set or not.
In a typical machine learning classification problem, an algorithm can be trained on a the features of node pairs in the test set and evaluated on features of node pairs in the test set. 
This method requires examples of both categories, meaning node pairs with an edge existing between them and not.
One of the large problems that other researchers have encounterd is a large class skew\cite{hassansurvey}.
Most collaboration networks, including the one studied, are quite sparse.
The arXiv high energy physics database graph has 17390 edges for 9066 nodes.
This situation can be problematic for some classifiers, as the distribution of the larger class can completely mask the distribution of the smaller class in the co-variate space.
A standard approach in statistics for dealing with this problem is to sample the more frequently occurring class, including fewer examples in the machine learning dataset\cite{rarity}.
Each fold contained around 11000 non-existing edges and 3000 existing edges.
When generating the testing and training sets, only 1/500 pairs of nodes with no edge between them were randomly selected for inclusion.
We also repeated the test with 1/100 random sampling, and the results did not change.

We tested several machine learning algorithms, including parametric and non-parametric models.
Simple parametric models like Linear Discriminant Analysis and General Linear Models for classifications are not appropriate in this application because the features are highly colinear.
This makes the matrices that the matrices that these algorithms need to invert are close to singluar, making results unreliable.
Stochastic Gradient Decent (SGD) was used instead of these models.
Non-parametric models tested are, Support Vector Machines (SVM), Random Forests, boosted stumps, and K-Nearest-Neighbors (KNN).

To evaluate the performance of classifiers, we used ten-fold cross-validation.
The data, both existing edges and down sampled non-existing edges, were randomly separated into ten sets.
The parallel edges in that represented multiple publications between authors were treated individually in this random selection, meaning that the algorithm predicted both new collaborations and repeated collaborations. 
For each fold of the cross-validation was used as a test set once.
We constructed weighted and unweighted graphs and using nine training folds and extracted the features for node pairs in the test and training sets from these graphs.
We then trained the algorithms for on the nine test fold and predicted the left-out fold.

We performed all analysis with the numpy and scikit-learn packages in Python \cite{scikit-learn}.

% Stochastic gradient descrent once had 25% error, convergence problem
\section{Results}
All machine learning algorithms showed good prediction power, however, KNN was by far the best model.
The results of the best classifiers in each category are summarized in table \ref{table:results}.
Below, we dicuss in detail the performance of each classifier type.

\begin{description}
    \item[Tree-based classifiers:] These were the worst-performing classifiers of the group.
        Changing the number of trees and the depth of trees used did not dramatically improve performance. 
        One notable observation is that in every cross-validation fold, the error of boosted trees and random forests was the same.
        They likely made errors on the same predictions.
    \item[SVM:] These classifiers showed consistently mediocre performance in all tests.
    \item[SGD:] This algorithm showed very good classification error rate in many cross validation tests, reaching as low as 4\%.
        It showed a convergence problem in a few situations that led to 25\% error rate.
    \item[KNN:] This was by far the best classifier, using a single nearest neighbor produced a classification error rate of 4\%.
        Increasing the number of neighbor lowered the classification error rate until to plateaued at 50 neighbors.
\end{description}

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Classifier} & \textbf{SVM} & \textbf{SGD} & \textbf{Random Forest} & \textbf{Boosted Stumps} & \textbf{KNN 10} & \textbf{KNN 50} \\
\hline
\textbf{CER} & 0.09289 & 0.07244 & 0.1031 &  0.1031 & 0.04927 & \textbf{0.02975}\\
\hline
\end{tabular}
\caption{Average classification error rate (CER) over several runs of cross-validation for all classifiers}
\label{table:results}
\end{table}

\subsection{Feature Distribution}
Analysis of the distribution of features shows some interesting conclusions.
As shown in figure \ref{fig:adamicnegihbors}, the Adamic/Adar and common neighbor values of paris of nodes are highly correlated as is the same for the common secondary neighbors and Jaccard similarity of neighbors.
This makes them essentially interchangable, and removing any of them would likely have left some classifiers, like KNN and random forests, unaffected and would have slightly improved the performance of the rest.
The distribution also shows that any nodes with common neighbors are very likely to collaborate in the test interval.
This is likely a property of the collaboration network.

The Katz score is a very powerful classifier, and is surprisingly uncorrelated to path features.
One can see that exclusively nodes with a low Katz score did not coauthor in the scatterplot in figure \ref{fig:katz}.
This computation, however, requires inverting the adjacency matrix of the graph, which may not be feasible for a large graph.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{adamic_common.png}
\caption{Distribution of Adamic/Adar and common neighbors for examples of non-coauthoring pairs (blue) and coauthoring pairs (red)}
\label{fig:adamicnegihbors}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{weight_unweight.png}
\caption{Distribution of weighted and unweighted shortest path for examples of non-coauthoring pairs (blue) and coauthoring pairs (red)}
\label{fig:weightunweight}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{katz_weight.png}
\caption{Distribution of weighted shortest path and Katz centrality for examples of non-coauthoring pairs (blue) and coauthoring pairs (red)}
\label{fig:katz}
\end{figure}

Predictably, unweighted shortest path and weighted weighted shortest path are correlated, but there is a clear trend that authors separated by the same number of hops are more likely to collaborate if they have a path with more collaborations joining them, the distribution is shown in figure \ref{fig:weightunweight}.

\section{Clustering Methodology}

Writing style of authors is considered as a behavioral biometric \cite{juola:2006}. The selection of words, the structure of sentences and frequency of words are potential alternatives for finding patterns and identifying the author of a piece of note. In a high-level view, there are two potential solutions for our project:

\begin{enumerate}
\item Supervised Learning: In this direction, we need to have ground truth from other resources. For instance, having more samples from each author such that we have clear labels to apply classification techniques.  
\item Unsupervised Learning: Unlike the previous approach, it is not needed to have ground truth and therefore the analysis techniques (e.g., clustering) could be conducted without the ground truth.
\end{enumerate}

The dataset used for clustering experiments was the same as the one used for link prediction part. We chose to have unsupervised learning for this project as in our data we did not have labels such that we could apply classification techniques. However, it does not mean that supervised learning was not possible. 

\subsection{Pre-processing}

Regarding that we had latex sources of publications in our dataset, we needed to extract pure text excluding the latex markup.
That is because we wanted to consider the whole text of papers for language processing and amount of latex markup was notable in this case and we could not ignore them.
Thus, we used ``opendetex'' \cite{opendetex} in order to strip Tex/LaTeX markup from the files in our dataset.

\subsection{Feature Extraction}

After having the data prepared for our analysis, we decided to use a set of Python libraries that could help us during implementation of our ideas. The libraries we used were as follows: NLTK \cite{nltk}, scikit-learn\cite{scikit-learn}, Scipy \cite{scipy}, and Numpy \cite{numpy}. We divided the dataset into different text files and then loaded the text files. Next, we wanted to perform language processing on the text files and extract features form them. For this aim, we first looked at the literature \cite{stamatatos:2009} to know more about recommended features. There are different possible features proposed by literature and we chose a set of them (explained later) with respect to two criteria:
\begin{enumerate}
\item We were looking for topic-independent as authors may write in different topics even if they do research in a research area. An example would a researcher in computer science who does research in algorithms both in theoretical and practical aspects.
\item We also wanted to have features that can distinctively capture an author's writing style.
\end{enumerate}

Therefore, we chose to have three types of features as follows:

\subsubsection{Punctuation Features}
For this group, we extracted features such as: Average number of commas, average number of semicolons, and average number of colons per sentence.
\subsubsection{Lexical Features}
We extracted the following lexical features as well: The average number of words per sentence, sentence length variation, and lexical diversity (i.e. variety of author's words).
\subsubsection{Bag of Words Features}
Bag of words features basically represent the frequency of different words in each paper. We extracted numerical features from text content following three steps:
\begin{enumerate}
\item We tokenized the string and gave an id for each possible token using token separators (e.g., space).
\item Next, we counted the token frequencies in the text.
\item At last, we normalized the numerical values.
\end{enumerate}

With this approach, frequency of every single token is assumed as a feature and therefore each paper can be represented by a matrix of features containing a row for each paper and one column per token. Please note that our vocabulary is the most common words across all the papers with this assumption that authors of papers consistently use these words in an special manner.

This approach is also called as ``Bag of n-grams'' and occurrences of words is the main criterion without considering the relative position information of the words in the papers.

\subsection{Clustering} \label{clustering}

For clustering, we decided to use k-means algorithms as it is used for text clustering in other papers \cite{aggarwal:2012}. As a brief explanation, we started by setting a value for k and then initialized clusters by picking one point per cluster. E.g., we picked a point randomly and then (k-1) other points like this such that each one placed as far as possible from the previous points (assuming Euclidian space). Then, for each point we placed it in the cluster whose current centroid was nearest and updated centroid of the cluster. After all points were assigned, we fixed the centroids of the k clusters. We also re-assigned all points to their closest centroids.

As mentioned, an important step in use of k-means is to specify the value for k, which is actually the number of clusters. In our case, we decided to choose k such it is equivalent to number of all authors. We believe that this could be beneficial in two ways. If we define k as mentioned, we can always look at papers assigned to each cluster and find the dominant author of the cluster. This way we can assume that this cluster represents the style of its dominant author. This interpretation could help in different ways: First of all, if there are multiple authors with similar names, by clustering we can find which paper belongs to whom. On the other hand, for all papers that dominant author was appeared as a co-author, we can consider more chance for those papers to be written (in terms of writing style) by the dominant author. Of course the better features we have, the better clusters are developed and these conclusions make more sense. Also, for the rest of papers assigned to a cluster, in which the dominant author is not involved, we can make a conclusion that their authors have similar writing style as the dominant author. This could be also helpful for at least decreasing the problem space in case of plagiarism detection.

For our clustering, we calculated the following measure for each cluster:
\begin{equation*}
\frac{\# P_{dominant}}{\Sigma P_{i} (P_{i} \in Cluster)}
\end{equation*}
Where $\# P_{dominant}$ is the total number of paper in which dominant author of cluster was involved and the denominator is the total number of papers assigned to the cluster. According to our analysis, the highest rate we could reach was 34\%, which makes us hopeful for even better rates in case of having more and better features. This rate could be interpreted as level of certainty for which we can claim the papers (that dominant author was involved) was actually written by the dominant author of this cluster. 

We also calculated the following measure for each $A_{i}$ $(P_{i} \in Cluster$ and $ P_{i} \notin P_{dominant})$:
\begin{equation*}
\frac{\# P_{i}}{\# P_{dominant}}
\end{equation*} 
Please note that for each cluster, we calculate ratio of total number of papers for each author that the dominant author is not involved to number of papers for dominant author. This measure could be considered as a similarity rate for writing styles of authors. According to the results, we reached 75\% rate.  

There are limitations in this regard that we discuss them in the limitation section of the report.

\section{Limitations}
In terms of clustering part, one limitation is that features might not be able to distinctively distinguish between writing styles. This is a general issue with feature extraction in unsupervised learning (especially in the context of text processing). Therefore, it is expected that better features could improve the results of our approach.

On the other hand, writing style of papers are unlikely to be purely preserved due to possible collaborations between co-authors, edits performed by co-authors, and even sterilization by professional copy editors. 


% Future work: determining which features are important, new, better features (keywords, related fields)
\section{Conclusion and Future Work}

In this project we aimed to apply link prediction and clustering technique to tackle the problem of name disambiguation and plagiarism detection in online collaboration networks.
Using a novel representation of collaboration graph, we reached collaboration prediction accuracy of more than 97\% under cross-validation.
We also could develop clusters to represent writing style of each author in our dataset such that it helps finding the right author in case of name ambiguity.
Therefore, both textual analysis and link prediction could be useful for name disambiguation.

In terms of future work, there are directions that could be investigated.
First one is to try finding better features from text such as syntactic features could help differentiate all authors of a publications.
Link prediction was very robust in this collaboration network, simple path and neighborhood features showed very high predictive power.
It remains to test whether this is a property of all collaborations networks or just the one studied.


\section{Teamwork Information}
During this project, we decided to discuss every single aspect of our project together. Therefore, we had regular meetings to chat about the project. As the result of our discussions, we agreed on the approaches, analysis, and implementation of the project from the beginning till the end. We also divided the implementations into two parts to balance the work load in this regard as well. Therefore, Hootan took care of implementation for clustering and Rudi took care of implementation for Link Prediction. In general, we both believe that we learned lessons from this project and it was fun! Also, we would like to thank Laks for his insightful comments on our project.

\bibliographystyle{plain}
%\bibliography{hassan,linksurvey,kleinberg,anothersurvey,katz,rarity,scikit}
\bibliography{references}
\end{document}

