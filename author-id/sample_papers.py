#!/usr/bin/python

import math
import random
from collections import defaultdict

from database_operations import *
from pprint import pprint


def sample_publications(db, n_folds):
    con, cursor = connectdb(db)

    pubs = defaultdict(list)

    cursor.execute("SELECT MAX(id) FROM authors")
    num_authors = cursor.fetchall()[0][0]
    print "Number of authors:", num_authors
    cursor.execute("SELECT COUNT(*) FROM publications")
    num_pubs = cursor.fetchall()[0][0]
    print "Number of collaborations:", num_pubs

    # Query all edges
    cursor.execute("SELECT * FROM publications")

    # Gotta fetch 'em all
    # This makes a list of authors that contributed to each publication
    while True:
        pub = cursor.fetchone()
        if not pub:
            con.close()
            break
        else:
            pubs[int(pub[1])].append(int(pub[0]))

    # Assign each publication to a fold
    pub_folds = { i: {} for i in range(n_folds) }
    for p in pubs:
        fold = random.choice(range(n_folds))
        pub_folds[fold][p] = pubs[p]

    return pub_folds
