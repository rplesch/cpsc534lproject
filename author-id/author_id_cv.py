#!/usr/bin/python

import MySQLdb as mdb
import numpy
from collections import defaultdict
from pprint import pprint
from database_operations import *
from sample_papers import *

class cv_tester:
    def __init__(self, db, n_folds, n_features):
        self.con, self.cursor = connectdb(db)
        self.paper_features = defaultdict(list)
        self.num_features = n_features

        self.cursor.execute("SELECT MAX(id) FROM authors")
        self.num_authors = self.cursor.fetchall()[0][0]
        print "Number of authors:", self.num_authors
        self.cursor.execute("SELECT COUNT(*) FROM publications")
        self.num_pubs = self.cursor.fetchall()[0][0]
        print "Number of collaborations:", self.num_pubs

        self.folds = sample_publications(db, n_folds)


    def get_all_paper_features(self, file_list):
        for feature_file in file_list:
            f = open(feature_file, 'r')
            for line in f:
                if line[0] is not '#' and len(line)>1:
                    data = line[:-1].split()
                    self.paper_features[int(data[0])] += [float(n) for n in data[1:]]


    def get_features_profile(self, ommitted_fold):
        training_profiles = defaultdict(self.empty_feature_factory)
        profile_weights_sum = defaultdict(self.zero_float_factory)

        for fold in [f for f in self.folds.keys() if f is not ommitted_fold]:
            for paper in self.folds[fold].keys():
                for author in self.folds[fold][paper]:
                    num_authors = len(self.folds[fold][paper])
                    profile_weights_sum[author] += 1/float(num_authors)
                    features = numpy.array(self.paper_features[paper]) / float(num_authors)
                    training_profiles[author] = numpy.add(training_profiles[author], features)

        for author in training_profiles.keys():
            training_profiles[author] = training_profiles[author] / profile_weights_sum[author]

        return training_profiles

    
    def predict_fold(self, test_fold, testk=[5,10,20,50]):
        author_profiles = self.get_features_profile(test_fold)
        maxk = max(testk)
        num_papers = float(len(self.folds[test_fold].keys()))
        true_positives = { k : 0 for k in [5, 10, 20, 50] }
        successfully_guessed = { k : 0 for k in [5, 10, 20, 50] }
        false_positive = { k : 0 for k in [5, 10, 20, 50] }
        false_negative = { k : 0 for k in [5, 10, 20, 50] }

        # my polynomials have polynomials
        for paper in self.folds[test_fold].keys():
            topk = []
            for profile in author_profiles.keys():
                distance = numpy.linalg.norm(numpy.subtract(self.paper_features[paper],author_profiles[profile]))
                if len(topk) < maxk:
                    topk.append((distance,profile))
                elif distance < topk[-1][0]:
                    topk = topk[:-1] + [(distance, profile)]
                    topk.sort(key=lambda author: author[0])

            true_authors = set(self.folds[test_fold][paper])
            for k in testk:
                guessed_authors = set([guess[1] for guess in topk[k:]])
                true_positive = true_authors & guessed_authors
                true_positives[k] += len(true_positives) / num_papers
                successfully_guessed[k] += len(true_positive) / len(true_authors) / num_papers

        return successfully_guessed, true_positives


    def get_clustering_features(self, folds, ommitted_fold, file_list):
        training_profiles = []
        training_clusters = []
        print 'Not implemented'


    def empty_feature_factory(self):
        return numpy.zeros(self.num_features)

    # What is this? Enterprise Java?
    def zero_float_factory(self):
        return float(0)
