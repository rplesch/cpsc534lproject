#!/usr/bin/python
#
#   Script for filling a table with all the authors in the dataset and linking each one to a unique ID, which we'll
# then use to reference the author in each of the other tables. We'll also have a collumn for comma separated calues of
# publication IDs.


import MySQLdb as mdb
import sys

from name_processor import author_processor

def insert_author(author, pub_id, cursor):
    # Still sexy insert string
    sql_insert = "INSERT INTO authors(name,publications) VALUES('{name}', '{pubs}')"
    sql_check = "SELECT * FROM authors WHERE name = '{name}'"
    sql_update = "UPDATE authors set publications = '{pubs}' WHERE name = '{name}'"

    # Check if collaboration allready exists
    author_exists = cursor.execute(sql_check.format(name=author))

    # Add a new edge
    if not author_exists:
        cursor.execute(sql_insert.format(name=author, pubs=pub_id))
    
    # Update existing edge
    else:
        author_row = cursor.fetchall()
        publications = author_row[0][2] + ',' + pub_id

        cursor.execute(sql_update.format(pubs=publications, name=author))


def insert_pub(pub, author, year, cursor):
    sql_insert = "INSERT INTO publications(pub_id,author_id,year) VALUES({pid}, {authors}, {pub_year})"
    sql_idcheck = "SELECT * FROM authors WHERE name = '{name}'"

    cursor.execute(sql_idcheck.format(name=author))
    row = cursor.fetchall()
    cursor.execute(sql_insert.format(pid=pub,authors=int(row[0][0]),pub_year=year))


# Open connection to database
try:
    con = mdb.connect('localhost', 'rudi', 'password', 'collaboration_test')

    cursor = con.cursor()
    cursor.execute("SELECT VERSION()")

    version = cursor.fetchone()

    print "Database version %s " %version


    with con:
        #Create table in database
        cursor.execute("DROP TABLE IF EXISTS authors")
        print 'dropped authors table'
        cursor.execute("CREATE TABLE authors(\
                        id INTEGER PRIMARY KEY AUTO_INCREMENT, \
                        name VARCHAR(25), \
                        publications VARCHAR(1000))")
        print 'created authors table'

        cursor.execute("DROP TABLE IF EXISTS publications")
        print 'dropped publications table'
        cursor.execute("CREATE TABLE publications(\
                        author_id INTEGER,\
                        pub_id INTEGER,\
                        year VARCHAR(2))")
        print 'created publications table'

    # Open authors file
    with open('datasets/all-authors.txt', 'r') as author_file:
        for line in author_file:
            # Split PubID from Authors
            pub_id = line.split('\t')[0]
            year = pub_id[:2]
            # Get rid of linebreaks and  split authors on commas
            authors = con.escape_string(line.split('\t')[1].replace('\n','')).split(',')

            # Extract parsed author names
            parsed_authors = author_processor(authors)
            
            # Put them in the dataset
            for author in parsed_authors:
                insert_author(author, pub_id, cursor)
                insert_pub(pub_id, author, year, cursor)

    # It's over now
    author_file.closed
    con.commit()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

    if con:
        con.rollback()

if con:
    con.close()
