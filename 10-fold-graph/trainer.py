#!/usr/bin/python

from edge_sampler import *
from feature_builder import featureSetTable
from pprint import pprint
from subprocess import call

from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC

class trainer:

    def __init__(self, db):
        call(['date'])
        self.db = db
        self.edge_folds = sample_edges(db, 10)
        

    def machine_learning(self, ommitted_fold):
        errors = {}

        print '**** Starting machine learning, test fold:', ommitted_fold, '****'
        call(['date'])
        test_features, test_classes, train_features, train_classes = self.get_features(ommitted_fold)
        
        knn_error50 = self.knn(test_features, test_classes, train_features, train_classes, neighbors=50)
        errors['knn50'] = knn_error50
        print 'Done 5 KNN. CV error is', knn_error50
        call(['date'])
        knn_error40 = self.knn(test_features, test_classes, train_features, train_classes, neighbors=40)
        errors['knn40'] = knn_error40
        print 'Done 1 KNN. CV error is', knn_error40
        call(['date'])
        knn_error100 = self.knn(test_features, test_classes, train_features, train_classes, neighbors=100)
        errors['knn100'] = knn_error100
        print 'Done 100 KNN. CV error is', knn_error100
        call(['date'])

        svm_error = self.svm(test_features, test_classes, train_features, train_classes)
        errors['svm'] = svm_error
        print 'Done SVM. CV error is', svm_error

        sgd_error = self.sgd(test_features, test_classes, train_features, train_classes)
        errors['sgd'] = sgd_error
        print 'Done SGD. CV error is', sgd_error

        return errors


    def get_features(self, ommitted_fold):
    # Doing this in a separate function so goes out of scope and doesn't sit in memory
        print 'Starting feature getting'
        call(['date'])
        feature_builder = featureSetTable(self.db, self.edge_folds, ommitted_fold)
        feature_builder.sample_edges()
        feature_builder.add_unweighted_shortest_path()
        print 'Done unweighted shortest path'
        call(['date'])
        feature_builder.add_weighted_shortest_path()
        print 'Done weighted shortest path'
        call(['date'])
        feature_builder.add_common_neighbors()
        feature_builder.add_neighbor_jaccard()
        feature_builder.add_common_2_neighbors()
        feature_builder.adamic_adar()
        print 'Done neighborhood things'
        call(['date'])
        feature_builder.unweighted_katz()
        print 'Done katz'
        call(['date'])

        train_features = []
        train_classes = []
        test_features = []
        test_classes = []

        for start_node in feature_builder.training:
            for end_node in feature_builder.training[start_node]:
                feat = [
                    feature_builder.training[start_node][end_node]['unweighted'],
                    feature_builder.training[start_node][end_node]['weighted'],
                    feature_builder.training[start_node][end_node]['common'],
                    feature_builder.training[start_node][end_node]['jaccard'],
                    feature_builder.training[start_node][end_node]['adamic'],
                    feature_builder.training[start_node][end_node]['common2'],
                    feature_builder.training[start_node][end_node]['katz']
                ]

                train_features.append(feat)

                train_classes.append(feature_builder.training[start_node][end_node]['exists'])
        for start_node in feature_builder.test:
            for end_node in feature_builder.test[start_node]:
                feat = [
                    feature_builder.test[start_node][end_node]['unweighted'],
                    feature_builder.test[start_node][end_node]['weighted'],
                    feature_builder.test[start_node][end_node]['common'],
                    feature_builder.test[start_node][end_node]['jaccard'],
                    feature_builder.test[start_node][end_node]['adamic'],
                    feature_builder.test[start_node][end_node]['common2'],
                    feature_builder.test[start_node][end_node]['katz']
                ]

                test_features.append(feat)

                test_classes.append(feature_builder.test[start_node][end_node]['exists'])

        print 'Train:', len(train_classes), sum(train_classes)
        print 'Test:', len(test_classes), sum(test_classes)

        return test_features, test_classes, train_features, train_classes

    def svm(self, test_features, test_classes, train_features, train_classes):
        classifier = SVC()
        classifier.fit(train_features, train_classes)
        predictions = classifier.predict(test_features)

        error_rate = sum([int(p[0]!=p[1]) for p in zip(predictions, test_classes)]) / float(len(predictions))
        return error_rate


    def sgd(self, test_features, test_classes, train_features, train_classes):
        classifier = SGDClassifier(loss='log')
        classifier.fit(train_features, train_classes)
        predictions = classifier.predict(test_features)

        error_rate = sum([int(p[0]!=p[1]) for p in zip(predictions, test_classes)]) / float(len(predictions))
        return error_rate
        

    def knn(self, test_features, test_classes, train_features, train_classes, neighbors=5):
        classifier = KNeighborsClassifier(n_neighbors = neighbors)
        classifier.fit(train_features, train_classes)
        predictions = classifier.predict(test_features)

        error_rate = sum([int(p[0]!=p[1]) for p in zip(predictions, test_classes)]) / float(len(predictions))
        return error_rate

    def random_forest(self, test_features, test_classes, train_features, train_classes, trees=100):
        classifier = RandomForestClassifier(n_estimators=trees, n_jobs=8)
        classifier.fit(train_features, train_classes)
        predictions = classifier.predict(test_features)

        error_rate = sum([int(p[0]!=p[1]) for p in zip(predictions, test_classes)]) / float(len(predictions))
        return error_rate

    def boost_trees(self, test_features, test_classes, train_features, train_classes, trees=200, depth=1):
        classifier = AdaBoostClassifier(DecisionTreeClassifier(max_depth=depth), algorithm='SAMME', n_estimators=trees)
        classifier.fit(train_features, train_classes)
        predictions = classifier.predict(test_features)

        error_rate = sum([int(p[0]!=p[1]) for p in zip(predictions, test_classes)]) / float(len(predictions))
        return error_rate
