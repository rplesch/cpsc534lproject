Collaboration Network Authorship Identification
===

This is the repository for all the code we use in the Social Computing project.

## Datasets

The URLs for downloading the datasets are in the `dataset-urls` file. To download the datasets, you can pipe this file to `wget`.

Each paper is tagged with a number. The tarball contains a file of paper IDs, a tab and the authors separated by commas. There's special characters in LaTeX markup for now, but I'll deal with this.

## Database

In it's current existence the database contains two table. One maps authors to a unique ID and stores all their publications in the database, the other stores a the edges of the collaboration graph. There will probably be a third soon, which will be the feature set to use in the link prediction training.

## Script

They are Python using MySQLdb, there's scripts for doing all manner of fun and useful things.