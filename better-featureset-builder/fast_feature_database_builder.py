#!/usr/bin/python

from database_operations import *
from itertools import combinations
from graph_maker import collaborationGraphs

from pprint import pprint
from numpy import matrix
from numpy import linalg
import sys
import math

class featureSetTable:

    def __init__(self, db):
        print 'Dis da remix baby!!'
        self.con, self.cursor = connectdb(db)
        self.db = db

        # Get number of edges and authors
        self.cursor.execute("SELECT MAX(id) FROM authors")
        self.num_authors = self.cursor.fetchall()[0][0]
        print "Number of authors:", self.num_authors
        self.cursor.execute("SELECT MAX(id) FROM edges")
        self.num_edges = self.cursor.fetchall()[0][0]
        print "Number of edges:", self.num_edges
        
        self.graphs = collaborationGraphs(self.db)
        print "Made graphs"
        
        
    def populate_table(self):
        sql_insert = "INSERT INTO featureset(does_exist,start_node,end_node) \
                    VALUES({exists},{start},{end})"
        self.cursor.execute("DROP TABLE IF EXISTS featureset")
        print 'dropped feature set table'
        self.cursor.execute("CREATE TABLE featureset(\
                        does_exist BOOLEAN, \
                        start_node INTEGER, \
                        end_node INTEGER, \
                        unweighted_path INTEGER, \
                        weighted_path FLOAT, \
                        common_neighbors INTEGER, \
                        common_2_neighbors INTEGER, \
                        neighbor_jaccard FLOAT, \
                        adamic FLOAT, \
                        katz FLOAT)")
        print 'created feature set table'

        # For the machine learning algorithm we want all possible edges
        # Build table so smaller edge id is always first
        for count, comb in enumerate(combinations(range(1,self.num_authors+1), 2)):
            
            if count % 10000 is 0:
                self.con.commit()
            if self.graphs.unweighted_graph.has_edge(comb):
                self.cursor.execute(sql_insert.format(exists=1,start=comb[0],end=comb[1]))

            else:
                self.cursor.execute(sql_insert.format(exists=0,start=comb[0],end=comb[1]))


        self.con.commit()

    def add_unweighted_shortest_path(self):
        add_path = "UPDATE featureset SET unweighted_path={length} \
                    WHERE start_node={start} AND end_node={end}"
        
        for count, start_node in enumerate(range(1,self.num_authors+1)):
            if count % 10 is 0:
                self.con.commit()
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            for end_node in [i for i in range(1,self.num_authors+1) if i!=start_node]:
                if end_node not in possible_paths:
                    self.cursor.execute(add_path.format(length=1000000000,start=start_node,end=end_node))
                else:
                    self.cursor.execute(add_path.format(length=possible_paths[end_node],start=start_node,end=end_node))

        self.con.commit()

    def add_weighted_shortest_path(self):
        add_path = "UPDATE featureset SET weighted_path={length} \
                    WHERE start_node={start} AND end_node={end}"

        for count, start_node in enumerate(range(1,self.num_authors+1)):
            if count % 10 is 0:
                self.con.commit()
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            possible_paths = self.graphs.weighted_shortest_paths(start_node)
            for end_node in [i for i in range(start_node,self.num_authors+1) if i!=start_node]:
                if end_node not in possible_paths:
                    self.cursor.execute(add_path.format(length=1000000000,start=start_node,end=end_node))
                else:
                    self.cursor.execute(add_path.format(length=possible_paths[end_node],start=start_node,end=end_node))
            

        self.con.commit()

    def add_common_neighbors(self):
        add_common = "UPDATE featureset SET common_neighbors={com} WHERE start_node={start} AND end_node={end}"
        
        for count, start_node in enumerate(range(1,self.num_authors+1)):
            if count % 10 is 0:
                self.con.commit()
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in [i for i in range(start_node,self.num_authors+1) if i!=start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                common = len(start_neighbors & end_neighbors)
                self.cursor.execute(add_common.format(com=common,start=start_node,end=end_node))


        self.con.commit()
    
    def add_neighbor_jaccard(self):
        add_jaccard = "UPDATE featureset SET neighbor_jaccard={jac} WHERE start_node={start} AND end_node={end}"


        for count, start_node in enumerate(range(1,self.num_authors+1)):
            if count % 10 is 0:
                self.con.commit()
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in [i for i in range(start_node,self.num_authors+1) if i!=start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                jaccard = len(start_neighbors & end_neighbors)/float(len(start_neighbors | end_neighbors))
                self.cursor.execute(add_jaccard.format(jac=jaccard,start=start_node,end=end_node))


        self.con.commit()

    def adamic_adar(self):
        add_adamic = "UPDATE featureset SET adamic={aa} WHERE start_node={start} AND end_node={end}"
        


        for count, start_node in enumerate(range(1,self.num_authors+1)):
            if count % 10 is 0:
                self.con.commit()
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in [i for i in range(start_node,self.num_authors+1) if i!=start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
        
                adamic = 0
                
                for common_neighbor in start_neighbors & end_neighbors:
                    adamic = adamic + 1/math.log(len(self.graphs.unweighted_graph.neighbors(common_neighbor)))

                self.cursor.execute(add_adamic.format(aa=adamic,start=start_node,end=end_node))


        self.con.commit()


    def add_common_2_neighbors(self):
        add_common = "UPDATE featureset SET common_2_neighbors={com} WHERE start_node={start} AND end_node={end}"
        
        
        for count, start_node in enumerate(range(1,self.num_authors+1)):
            if count % 10 is 0:
                self.con.commit()
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            start_2_neighbors = set()
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for neigh in start_neighbors:
                start_2_neighbors = start_2_neighbors | set(self.graphs.unweighted_graph.neighbors(neigh))
            for end_node in [i for i in range(start_node,self.num_authors+1) if i!=start_node]:
                end_2_neighbors = set()
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
                for end_neigh in end_neighbors:
                    end_2_neighbors = end_2_neighbors | set(self.graphs.unweighted_graph.neighbors(end_neigh))

                common = len(start_2_neighbors & end_2_neighbors)
                self.cursor.execute(add_common.format(com=common,start=start_node,end=end_node))


        self.con.commit()
    
                
    def unweighted_katz(self):
        add_katz = "UPDATE featureset SET katz={kz} WHERE start_node={start} AND end_node={end}"

        adjacency_matrix = matrix(self.graphs.get_adjacency_matrix())

        ident = [[0 for i in range(len(adjacency_matrix))] for j in range(len(adjacency_matrix))]
        for i in range(len(adjacency_matrix)):
            ident[i][i] = 1
        identity = matrix(ident)

        katz = (identity - 0.1*adjacency_matrix).I - identity

        for comb in combinations(range(len(katz)),2):
            this_katz = katz[comb[0],comb[1]]
            self.cursor.execute(add_katz.format(kz=this_katz,start=comb[0]+1,end=comb[1]+1))


        self.con.commit()


    def drop_column(self, column):
        self.cursor.execute("ALTER TABLE featureset DROP COLUMN " + column)
