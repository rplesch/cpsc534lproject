#!/usr/bin/python

#   Script to open the database for testing new funcitonality

from fast_graph_maker import collaborationGraphs
from upsampled_onewrite_feature import featureSetTable
from pprint import pprint

from database_operations import *
from pprint import pprint
from subprocess import call

call(['date'])
print 'GO!'

the_table = featureSetTable('collaboration_test')

the_table.sample_edges()

the_table.add_unweighted_shortest_path()
call(['date'])
print 'Done unweighted shortest path'

the_table.add_weighted_shortest_path()
call(['date'])
print 'Done weighted shortest path'

the_table.add_common_neighbors()
call(['date'])
print 'Done common neighbors'

the_table.add_neighbor_jaccard()
call(['date'])
print 'Done jaccard'

the_table.adamic_adar()
call(['date'])
print 'Done adamic'

the_table.add_common_2_neighbors()
call(['date'])
print 'Done common 2 neighbors'

the_table.unweighted_katz()
call(['date'])
print 'Done katz'

#the_table.rooted_pagerank()
#call(['date'])
#print 'Done rooted pagerank'

the_table.dump_to_db()
print 'Done db dump'

call(['date'])
