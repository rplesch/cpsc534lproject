#!/usr/bin/python

# Pass all feature files as command line agruments to this function

from author_id_cv import cv_tester
import sys
from subprocess import call

if len(sys.argv)<2:
    print "Pass feature files as command line arguments"
    sys.exit(0)

cv_folds = 10

call(['date'])
print 'Building features'

c = cv_tester('collaboration_test', cv_folds, 6)
c.get_all_paper_features(sys.argv[1:])

for fold in range(cv_folds):
    print 'Starting fold', fold
    call(['date'])
    print c.predict_fold(fold)
