#!/usr/bin/python
#
#   A new vesrion of collaborationGraphs. Makes weighted and unweighted graphs of collaboration databases for
# calculating features.
#   The folds in trainer are a dictionary of the form:
# {fold: {start node: {end node: edge weight ...}...}...}
# constructor expects this.



import sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')
import gv
from pprint import pprint

from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.minmax import *
from pygraph.readwrite.dot import write


class collaborationGraphs:

    def __init__(self, edges, ommitted_fold, num_authors, draw=0):
        self.num_authors = num_authors

        self.unweighted_graph = graph()
        self.unweighted_graph.add_nodes(range(1,num_authors+1))
        self.weighted_graph = graph()
        self.weighted_graph.add_nodes(range(1,num_authors+1))

        all_edges = {}
        for fold in [i for i in edges.keys() if i is not ommitted_fold]:
            for start_node in edges[fold]:
                if start_node not in all_edges:
                    all_edges[start_node] = {}
                for end_node in edges[fold][start_node]:
                    if end_node not in all_edges[start_node]:
                        all_edges[start_node][end_node] = 1
                    else:
                        all_edges[start_node][end_node] += 1

        for start_node in all_edges:
            for end_node in all_edges[start_node]:
                self.weighted_graph.add_edge((start_node,end_node),1/float(all_edges[start_node][end_node]))
                self.unweighted_graph.add_edge((start_node,end_node))

        if draw:
            dot = write(self.unweighted_graph)
            gvv = gv.readstring(dot)
            gv.layout(gvv,'neato')
            gv.render(gvv,'png',db+'-neato.png')
    

    def unweighted_shortest_paths(self, start_node):
        return shortest_path(self.unweighted_graph, start_node)[1]


    def weighted_shortest_paths(self, start_node):
        return shortest_path(self.weighted_graph, start_node)[1]

    
    def get_adjacency_matrix(self):
        return [[int(self.weighted_graph.has_edge((start,end))) for end in range(1,self.num_authors+1)] for start in range(1,self.num_authors+1)]
