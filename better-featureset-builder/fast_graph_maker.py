#!/usr/bin/python

import sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')
import gv

from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.minmax import *
from pygraph.readwrite.dot import write

import MySQLdb as mdb
from database_operations import *

class collaborationGraphs:

    def __init__(self, db, weighted=1, unweighted=1, draw=0):
        self.unweighted_graph = graph()
        self.weighted_graph = graph()

        con, cursor = connectdb(db)

        # Get number of edges
        cursor.execute("SELECT MAX(id) FROM authors")
        self.num_authors = cursor.fetchall()[0][0]
        print "Number of authors:", self.num_authors

        # Only care about numerical id for nodes
        self.unweighted_graph.add_nodes(range(1, self.num_authors+1))
        self.weighted_graph.add_nodes(range(1, self.num_authors+1))

        cursor.execute("SELECT MAX(id) FROM edges")
        num_edges = cursor.fetchall()[0][0]
        print "Number of edges:", num_edges

        # Query all for edge in range(1,num_edges):
        cursor.execute("SELECT * FROM edges")

        # Gotta fetch 'em all
        while True:
            edge = cursor.fetchone()
            if not edge:
                break
            if weighted:
                self.weighted_graph.add_edge((int(edge[1]),int(edge[2])), 1/float(edge[3]))
            if unweighted:
                self.unweighted_graph.add_edge((int(edge[1]),int(edge[2])))
            else:
                print 'Niether weighted nor unweighted selected, made empty graphs'
                break

        if draw:
            dot = write(self.unweighted_graph)
            gvv = gv.readstring(dot)
            gv.layout(gvv,'neato')
            gv.render(gvv,'png',db+'-neato.png')
    

    def unweighted_shortest_paths(self, start_node):
        return shortest_path(self.unweighted_graph, start_node)[1]


    def weighted_shortest_paths(self, start_node):
        return shortest_path(self.weighted_graph, start_node)[1]

    
    def get_adjacency_matrix(self):
        return [[int(self.weighted_graph.has_edge((start,end))) for end in range(1,self.num_authors+1)] for start in range(1,self.num_authors+1)]
