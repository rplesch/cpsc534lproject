#!/usr/bin/python

import random
import math
from pprint import pprint
from database_operations import *

def sample_edges(db, n_folds):
    con, cursor = connectdb(db)

    edge_folds = { i: {} for i in range(n_folds) }

    cursor.execute("SELECT MAX(id) FROM authors")
    num_authors = cursor.fetchall()[0][0]
    print "Number of authors:", num_authors
    cursor.execute("SELECT MAX(id) FROM edges")
    num_edges = cursor.fetchall()[0][0]
    print "Number of edges:", num_edges

    # Query all edges
    cursor.execute("SELECT * FROM edges")

    # Gotta fetch 'em all
    while True:
        edge = cursor.fetchone()
        if not edge:
            con.close()
            break
    # edge weight is number of parallel edges, treat each of these indepentently
        start = int(edge[1])
        end = int(edge[2])
        for e in range(edge[3]):
            fold = random.choice(range(n_folds))
            if start not in edge_folds[fold]:
                edge_folds[fold][start] = {}
            if end not in edge_folds[fold][start]:
                edge_folds[fold][start][end] = 1
            else:
                edge_folds[fold][start][end] += 1

    return edge_folds
                
