
#   Functions to get data and fit a KNN classifier

from database_operations import *
from sklearn.ensemble import RandomForestClassifier
from pprint import pprint
import numpy as np
import math
import random

class randomForest:

    def __init__(self, db, n_trees, n_folds):
        print 'Making RandomForest classifier'
        self.con, self.cursor = connectdb(db)

        # make cross-validation folds
        self.n_folds = n_folds
        self.cv_covariate_folds = {fold: [] for fold in range(self.n_folds)}
        self.cv_class_folds = {fold: [] for fold in range(self.n_folds)}

        self.cursor.execute("SELECT exist, COUNT(*) FROM 100_featureset_training GROUP BY exist")
        numbers = self.cursor.fetchall()
        print numbers[0][1], "negative instances"
        print numbers[1][1], "positive instances"
        
        # Get all data points
        # We don't care about start and end points, hence datapoint[3:]
        # Also want to convert longs to ints
        self.cursor.execute("SELECT * FROM 100_featureset_training")
        while True:
            datapoint = self.cursor.fetchone()
            if not datapoint:
                break
            else:
                dp = list(datapoint[3:])
                for i in [0, 2, 3]:
                    dp[i] = int(dp[i])
                fold = random.choice(range(self.n_folds))
                self.cv_covariate_folds[fold].append(dp)
                self.cv_class_folds[fold].append(datapoint[0])

        self.con.close()

        self.classifier = RandomForestClassifier(n_estimators=n_trees, n_jobs=6)

    def cross_validation(self):
        print 'Cross-validating'
        cv_errors = []

        for fold in self.cv_covariate_folds:
            training_covariates = [data for i in range(self.n_folds) for data in self.cv_covariate_folds[i] if i is not fold]
            training_class = [data for i in range(self.n_folds) for data in self.cv_class_folds[i] if i is not fold]
            self.classifier.fit(training_covariates, training_class)

            predictions = self.classifier.predict(self.cv_covariate_folds[fold])
            cv_errors.append(sum([int(p[0]!=p[1]) for p in zip(predictions,self.cv_class_folds[fold])])/float(len(predictions)))

        print cv_errors

        mean_error = sum(cv_errors)/float(len(cv_errors))
        return mean_error

