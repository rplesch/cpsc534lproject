import os
import string
from collections import Counter

# Fill a list with all the paths of the abstract files
abstracts = []
for root, dirs, files in os.walk("."):
    for file in files:
        if file.endswith(".abs"):
            abstracts.append(os.path.join(root,file))


# For each abstract file, find the chunk after the last double slash
# and add each word to the histogram counter object
histogram = Counter()
for abstract in abstracts:
    abstract_text = open(abstract).read()
    words = abstract_text.replace('\n',' ').lower().split('\\\\')[-2]

    # This can (and should) be replaced with a nice regex call
    for special_char in string.punctuation:
        words = words.replace(special_char,' ')
    histogram.update(words.split())

most_common = set(histogram.most_common(2000))
keywords = set(histogram) - most_common
print keywords
