#!/usr/bin/python

from trainer import trainer
from pprint import pprint
from collections import defaultdict
import json

t = trainer('collaboration_test')
cv_error = defaultdict(list)

for fold in range(10):
    outputfile = open('bigknn-2/cv'+str(fold)+'.json', 'w')
    fold_error = t.machine_learning(fold)
    json.dump(fold_error, outputfile)
    outputfile.close()
    pprint(fold_error)

    for test in fold_error:
        cv_error[test].append(fold_error[test])

pprint(cv_error)
resultfile = open('bigknn-2/results.json', 'w')
json.dump(cv_error, resultfile)
resultfile.close()

mean_error = defaultdict(float)
for test in cv_error:
    mean_error[test] = sum(cv_error[test])/float(len(cv_error[test]))

pprint(mean_error)
meanerrorfile = open('bigknn-2/mean_error.json', 'w')
json.dump(mean_error, meanerrorfile)
meanerrorfile.close()
