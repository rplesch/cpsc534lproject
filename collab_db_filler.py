#!/usr/bin/python
#  Script for building collaboration graph in a database
# The data comes from the all-authors file which is in the repo
# The the database will have one line for each edge with the start node,
# end node, string of comma separated publications, and 
#
# This isn't what I'm going to use any more, the database will have multiple tables now, one for anuthors linking to an ID,
# and a separate one to store the graph edges

import MySQLdb as mdb
import sys
import re

from name_processor import author_processor
# this is an old version of edge_generator
#from edge_generator import edge_generator



def insert_edge(edge, pud_id, cursor):
    # Sexy insert string
    sql_insert = "INSERT INTO graph(start_node,end_node,weight,publications) VALUES('{start_node}', '{end_node}', {weight}, '{pid}')"
    sql_check = "SELECT * FROM graph WHERE start_node = '{start_node}' AND end_node = '{end_node}'"
    sql_update = "UPDATE graph set weight={weight}, publications='{pubs}' WHERE start_node = '{start_node}' AND end_node = '{end_node}'"

    # Check if collaboration already exists
    edge_exist = cursor.execute(sql_check.format(start_node=edge[0],end_node=edge[1]))
    edge_row = cursor.fetchall()

    # Add new edge
    if not edge_exist:
        cursor.execute(sql_insert.format(start_node=edge[0],end_node=edge[1],weight='1',pid=pub_id))

    # update existing edge
    else:
        publications = edge_row[0][4] + ',' + pub_id
        new_weight = edge_row[0][3] + 1

        cursor.execute(sql_update.format(weight=new_weight, pubs=publications, start_node=edge[0], end_node=edge[1]))




# Open connection to database
try:
    con = mdb.connect('localhost', 'rudi', 'password', 'collaboration_test')

    cursor = con.cursor()
    cursor.execute("SELECT VERSION()")

    version = cursor.fetchone()

    print "Database version %s " %version


    with con:
        #Create table in database
        cursor.execute("DROP TABLE IF EXISTS graph")
        print 'dropped graph table'
        cursor.execute("CREATE TABLE graph(\
                        id INTEGER PRIMARY KEY AUTO_INCREMENT, \
                        start_node VARCHAR(25), \
                        end_node VARCHAR(25), \
                        weight INTEGER, \
                        publications VARCHAR(1000))")
        print 'created graph table'

    # Open authors file
    with open('datasets/all-authors.txt', 'r') as author_file:
        for line in author_file:
            # Split PubID from Authors
            pub_id = line.split('\t')[0]
            # Get rid of linebreaks and  split authors on commas
            authors = con.escape_string(line.split('\t')[1].replace('\n','')).split(',')

            # Extract parsed author names
            parsed_authors = author_processor(authors)
            edge_set = edge_generator(parsed_authors)
            
            # Put them in the dataset
            for edge in edge_set:
                insert_edge(edge, pub_id, cursor)

    # It's over now
    author_file.closed
    con.commit()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

    if con:
        con.rollback()

if con:
    con.close()
