#!/usr/bin/python
#   The actual script that makes a table of all the edges in the collaboration graph.
# Each line in the table will have a the author ID of the start and end author and an edge weight, all as integers.
# We'll insert new edges with the smallest ID as the start node, and check that the same edge doesn't already exist.
# If it does we'll just increment the edge weight.'
#
#   This could be cleaned up by handling the incrementning on the SQL level, by making the primary key uniquely map
# to a pair of nodes and using icrement on duplicate, but I can't think of a nice hash now.


import MySQLdb as mdb
import sys

from edge_generator import edge_generator
from name_processor import author_processor


def insert_edge(edge, cursor):
    # Sexy insert string
    sql_insert = "INSERT INTO edges(start_node,end_node,weight) VALUES({start_node}, {end_node}, {weight})"
    sql_check = "SELECT * FROM edges WHERE start_node = {start_node} AND end_node = {end_node}"
    sql_update = "UPDATE edges set weight={weight} WHERE start_node = {start_node} AND end_node = {end_node}"

    # Check if collaboration already exists
    edge_exist = cursor.execute(sql_check.format(start_node=edge[0],end_node=edge[1]))

    # Add new edge
    if not edge_exist:
        cursor.execute(sql_insert.format(start_node=edge[0],end_node=edge[1],weight=1))

    # update existing edge
    else:
        edge_row = cursor.fetchall()
        new_weight = edge_row[0][3] + 1

        cursor.execute(sql_update.format(weight=new_weight, start_node=edge[0], end_node=edge[1]))



try:
    con = mdb.connect('localhost', 'rudi', 'password', 'toygraph')

    cursor = con.cursor()
    cursor.execute("SELECT VERSION()")

    version = cursor.fetchone()

    print "Database version %s " %version


    with con:
        #Create table in database
        cursor.execute("DROP TABLE IF EXISTS edges")
        print 'dropped edge table'
        cursor.execute("CREATE TABLE edges(\
                        id INTEGER PRIMARY KEY AUTO_INCREMENT, \
                        start_node INTEGER, \
                        end_node INTEGER, \
                        weight INTEGER)")
        print 'created edges table'

    with open('datasets/toy-graph.txt', 'r') as author_file:
        for line in author_file:
            # Split PubID from Authors
            pub_id = line.split('\t')[0]
            # Get rid of linebreaks and  split authors on commas
            authors = con.escape_string(line.split('\t')[1].replace('\n','')).split(',')

            # Extract parsed author names
            parsed_authors = author_processor(authors)
            edge_set = edge_generator(parsed_authors, cursor)
            
            # Put them in the dataset
            for edge in edge_set:
                insert_edge(edge, cursor)

    # It's over now
    author_file.closed
    con.commit()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

    if con:
        con.rollback()

if con:
    con.close()
