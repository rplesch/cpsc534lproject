#!/usr/bin/python

from database_operations import *
from itertools import combinations
from graph_maker import collaborationGraphs

from pprint import pprint
from numpy import matrix
from numpy import linalg
from numpy import newaxis
import numpy
import sys
import math
import random

class featureSetTable:

    def __init__(self, db):
        print 'Dis da remix baby!!'
        self.con, self.cursor = connectdb(db)
        self.db = db

        self.all_data= {}

        # Get number of edges and authors
        self.cursor.execute("SELECT MAX(id) FROM authors")
        self.num_authors = self.cursor.fetchall()[0][0]
        print "Number of authors:", self.num_authors
        self.cursor.execute("SELECT MAX(id) FROM edges")
        self.num_edges = self.cursor.fetchall()[0][0]
        print "Number of edges:", self.num_edges
        
        self.graphs = collaborationGraphs(self.db)
        print "Made graphs"
        
        
    def dump_to_db(self):
        self.cursor.execute("DROP TABLE IF EXISTS 100_featureset_training")
        self.cursor.execute("CREATE TABLE 100_featureset_training(\
                        exist BOOLEAN, \
                        start INTEGER, \
                        end INTEGER, \
                        unweighted INTEGER, \
                        weighted FLOAT, \
                        common INTEGER, \
                        common_2 INTEGER, \
                        jaccard FLOAT, \
                        adamic FLOAT, \
                        katz FLOAT)")

        insert_training = 'INSERT INTO 100_featureset_training\
                (start, end, exist, unweighted, weighted, common, common_2, jaccard, adamic, katz) \
                VALUES \
                ({start},{end},{exist},{unweighted},{weighted},{common},{common_2},{jaccard},{adamic},{katz})'

        self.cursor.execute("DROP TABLE IF EXISTS 100_featureset_testing")
        self.cursor.execute("CREATE TABLE 100_featureset_testing(\
                        exist BOOLEAN, \
                        start INTEGER, \
                        end INTEGER, \
                        unweighted INTEGER, \
                        weighted FLOAT, \
                        common INTEGER, \
                        common_2 INTEGER, \
                        jaccard FLOAT, \
                        adamic FLOAT, \
                        katz FLOAT)")

        insert_testing = 'INSERT INTO 100_featureset_testing\
                (start, end, exist, unweighted, weighted, common, common_2, jaccard, adamic, katz) \
                VALUES \
                ({start},{end},{exist},{unweighted},{weighted},{common},{common_2},{jaccard},{adamic},{katz})'

        for start_node in self.all_data:
            if start_node % 10 is 0:
                self.con.commit()

            for end_node in self.all_data[start_node]:
                if random.random() > 0.1:
                    self.cursor.execute(insert_training.format(start=start_node,end=end_node,
                        exist=self.all_data[start_node][end_node]['exists'],
                        unweighted=self.all_data[start_node][end_node]['unweighted'],
                        weighted=self.all_data[start_node][end_node]['weighted'],
                        common=self.all_data[start_node][end_node]['common'],
                        common_2=self.all_data[start_node][end_node]['common2'],
                        jaccard=self.all_data[start_node][end_node]['jaccard'],
                        katz=self.all_data[start_node][end_node]['katz'],
                        adamic=self.all_data[start_node][end_node]['adamic']))
                else:
                    self.cursor.execute(insert_testing.format(start=start_node,end=end_node,
                        exist=self.all_data[start_node][end_node]['exists'],
                        unweighted=self.all_data[start_node][end_node]['unweighted'],
                        weighted=self.all_data[start_node][end_node]['weighted'],
                        common=self.all_data[start_node][end_node]['common'],
                        common_2=self.all_data[start_node][end_node]['common2'],
                        jaccard=self.all_data[start_node][end_node]['jaccard'],
                        katz=self.all_data[start_node][end_node]['katz'],
                        adamic=self.all_data[start_node][end_node]['adamic']))

        self.con.commit()

    def sample_edges(self):
        # For the machine learning algorithm we want all possible edges
        # but we only take 1/500 non-existent edges
        for count, comb in enumerate(combinations(range(1,self.num_authors+1), 2)):
            
            if comb[0] not in self.all_data:
                self.all_data[comb[0]] = {}
            if self.graphs.unweighted_graph.has_edge(comb):
                if comb[1] not in self.all_data[comb[0]]:
                    self.all_data[comb[0]][comb[1]] = {}
                self.all_data[comb[0]][comb[1]]['exists']=1
            else:
                if random.random() < 0.01:
                    if comb[1] not in self.all_data[comb[0]]:
                        self.all_data[comb[0]][comb[1]] = {}
                    self.all_data[comb[0]][comb[1]]['exists']=0


    def add_unweighted_shortest_path(self):
        
        for start_node in self.all_data: 
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            for end_node in self.all_data[start_node]:
                if end_node not in possible_paths:
                    self.all_data[start_node][end_node]['unweighted'] = 1000000000
                else:
                    self.all_data[start_node][end_node]['unweighted'] = possible_paths[end_node] 


    def add_weighted_shortest_path(self):

        for start_node in self.all_data: 
            possible_paths = self.graphs.weighted_shortest_paths(start_node)
            for end_node in self.all_data[start_node]:
                if end_node not in possible_paths:
                    self.all_data[start_node][end_node]['weighted'] = 1000000000
                else:
                    self.all_data[start_node][end_node]['weighted'] = possible_paths[end_node] 
            

    def add_common_neighbors(self):
        
        for start_node in self.all_data:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.all_data[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                common = len(start_neighbors & end_neighbors)
                self.all_data[start_node][end_node]['common'] = common

    
    def add_neighbor_jaccard(self):

        for start_node in self.all_data:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.all_data[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                if len(start_neighbors | end_neighbors) is 0:
                    jaccard = 0
                else:
                    jaccard = len(start_neighbors & end_neighbors)/float(len(start_neighbors | end_neighbors))
                self.all_data[start_node][end_node]['jaccard'] = jaccard


    def adamic_adar(self):
        
        for start_node in self.all_data:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.all_data[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
        
                adamic = 0
                
                for common_neighbor in start_neighbors & end_neighbors:
                    adamic = adamic + 1/math.log(len(self.graphs.unweighted_graph.neighbors(common_neighbor)))

                self.all_data[start_node][end_node]['adamic'] = adamic


    def add_common_2_neighbors(self):
        
        for start_node in self.all_data:
            start_2_neighbors = set()
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for neigh in start_neighbors:
                start_2_neighbors = start_2_neighbors | set(self.graphs.unweighted_graph.neighbors(neigh))
            for end_node in self.all_data[start_node]:
                end_2_neighbors = set()
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
                for end_neigh in end_neighbors:
                    end_2_neighbors = end_2_neighbors | set(self.graphs.unweighted_graph.neighbors(end_neigh))

                common = len(start_2_neighbors & end_2_neighbors)
                self.all_data[start_node][end_node]['common2'] = common
    
                
    def unweighted_katz(self):

        adjacency_matrix = matrix(self.graphs.get_adjacency_matrix())

        ident = [[0 for i in range(len(adjacency_matrix))] for j in range(len(adjacency_matrix))]
        for i in range(len(adjacency_matrix)):
            ident[i][i] = 1
        identity = matrix(ident)

        katz = (identity - 0.01*adjacency_matrix).I - identity

        for start_node in self.all_data:
            for end_node in self.all_data[start_node]:
                this_katz = katz[start_node-1, end_node-1]
                self.all_data[start_node][end_node]['katz'] = this_katz


    def rooted_pagerank(self):
        
        probability = 0.2

        adjacency = matrix(self.graphs.get_adjacency_matrix())
        row_sums = adjacency.sum(axis=1)
        norm_adj = numpy.zeros(numpy.shape(adjacency))
        for i , (row, rowsum) in enumerate(zip(adjacency, row_sums)):
            if row_sums is 0:
                norm_adj[i,:] = 0
            else:
                norm_adj[i,:] = row / float(rowsum)

        identity = matrix(numpy.identity(numpy.shape(norm_adj)[0]))

        degree = [[0 for i in range(len(norm_adj))] for j in range(len(norm_adj))]
        for i, node in enumerate(self.all_data):
            neighbors = len(self.graphs.unweighted_graph.neighbors(node))
            if neighbors is 0:
                degree[i][i] = sys.maxint
            else:
                degree[i][i] = 1/float(neighbors)
        degree_mat = matrix(degree)

        RPR = (1-probability)*((identity-probability*(degree*norm_adj)).I)
        
        for start_node in self.all_data:
            for end_node in self.all_data[start_node]:
                self.all_data[start_node][end_node]['rpr'] = RPR[start_node-1,end_node-1]



    def drop_column(self, column):
        self.cursor.execute("ALTER TABLE featureset DROP COLUMN " + column)
