#!/usr/bin/python
#
#   Script to open the database for testing new funcitonality

from fast_feature_database_builder import featureSetTable

#call(['date'])
print 'GO!'

the_table = featureSetTable('collaboration_test')

#print 'Starting table populate'
#the_table.populate_table()
#print 'Done table populate'

#the_table.drop_column('unweighted_path')
#the_table.add_unweighted_shortest_path()
#print 'Done unweighted shortest path'

#the_table.drop_column('weighted_path')
the_table.add_weighted_shortest_path()
print 'Done weighted shortest path'

#the_table.drop_column('common_neighbors')
#the_table.add_common_neighbors()
#print 'Done common neighbors'

#the_table.drop_column('neighbor_jaccard')
#the_table.add_neighbor_jaccard()
#print 'Done jaccard'

#the_table.drop_column('adamic')
#the_table.adamic_adar()
#print 'Done adamic'

#the_table.drop_column('common_2_neighbors')
#the_table.add_common_2_neighbors()
#print 'Done common 2 neighbors'

#the_table.drop_column('katz')
#the_table.unweighted_katz()
#print 'Done katz'
#call(['date'])
