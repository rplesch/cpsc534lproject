#!/usr/bin/python

from database_operations import *
from itertools import combinations
from graph_maker import collaborationGraphs

from pprint import pprint
from numpy import matrix
from numpy import linalg
from numpy import newaxis
import numpy
import sys
import math
import random

class featureSetTable:

    def __init__(self, db, existing_edges, ommitted_fold):
        print 'Dis da remix baby!!'
        self.con, self.cursor = connectdb(db)
        self.db = db
        self.ommitted_fold = ommitted_fold
        self.existing_edges = existing_edges

        self.training = {}
        self.test = {}

        # Get number of edges and authors
        self.cursor.execute("SELECT MAX(id) FROM authors")
        self.num_authors = self.cursor.fetchall()[0][0]
        print "Number of authors:", self.num_authors
        self.cursor.execute("SELECT MAX(id) FROM edges")
        self.num_edges = self.cursor.fetchall()[0][0]
        print "Number of edges:", self.num_edges
        
        self.graphs = collaborationGraphs(existing_edges, ommitted_fold, self.num_authors)
        print "Made graphs"
        
        
    def sample_edges(self):
        # For the machine learning algorithm we want all possible edges
        # but we only take 1/500 non-existent edges
        for count, comb in enumerate(combinations(range(1,self.num_authors+1), 2)):
            start = comb[0]
            end = comb[1]

            # check if edge in test fold
            if start in self.existing_edges[self.ommitted_fold]:
                if end in self.existing_edges[self.ommitted_fold][start]:
                    if start not in self.test:
                        self.test[start] = {}
                    if end not in self.test[start]:
                        self.test[start][end] = {}
                    self.test[start][end]['exists'] = 1

            # check if edge in training folds
            # only edges in training folds are added to graph
            if self.graphs.unweighted_graph.has_edge(comb):
                if start not in self.training:
                    self.training[start] = {}
                if end not in self.training[start]:
                    self.training[start][end] = {}
                self.training[start][end]['exists']=1
            
            # non-existent edge, we only want some of these so
            # we put 1/500 into training set and 1/5000 into test set
            else:
                roll = random.random()
                if roll < 0.0002:
                    if start not in self.test:
                        self.test[start] = {}
                    if end not in self.test[start]:
                        self.test[start][end] = {}
                    self.test[start][end]['exists']=0
                elif roll < 0.002:
                    if start not in self.training:
                        self.training[start] = {}
                    if end not in self.training[start]:
                        self.training[start][end] = {}
                    self.training[start][end]['exists']=0


    def add_unweighted_shortest_path(self):
        
        for start_node in self.training: 
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            for end_node in self.training[start_node]:
                if end_node not in possible_paths:
                    self.training[start_node][end_node]['unweighted'] = 1000000000
                else:
                    self.training[start_node][end_node]['unweighted'] = possible_paths[end_node] 
        for start_node in self.test: 
            possible_paths = self.graphs.unweighted_shortest_paths(start_node)
            for end_node in self.test[start_node]:
                if end_node not in possible_paths:
                    self.test[start_node][end_node]['unweighted'] = 1000000000
                else:
                    self.test[start_node][end_node]['unweighted'] = possible_paths[end_node] 


    def add_weighted_shortest_path(self):

        for start_node in self.training: 
            possible_paths = self.graphs.weighted_shortest_paths(start_node)
            for end_node in self.training[start_node]:
                if end_node not in possible_paths:
                    self.training[start_node][end_node]['weighted'] = 1000000000
                else:
                    self.training[start_node][end_node]['weighted'] = possible_paths[end_node] 
        for start_node in self.test: 
            possible_paths = self.graphs.weighted_shortest_paths(start_node)
            for end_node in self.test[start_node]:
                if end_node not in possible_paths:
                    self.test[start_node][end_node]['weighted'] = 1000000000
                else:
                    self.test[start_node][end_node]['weighted'] = possible_paths[end_node] 
            

    def add_common_neighbors(self):
        
        for start_node in self.training:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.training[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                common = len(start_neighbors & end_neighbors)
                self.training[start_node][end_node]['common'] = common
        for start_node in self.test:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.test[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                common = len(start_neighbors & end_neighbors)
                self.test[start_node][end_node]['common'] = common

    
    def add_neighbor_jaccard(self):

        for start_node in self.training:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.training[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                if len(start_neighbors | end_neighbors) is 0:
                    jaccard = 0
                else:
                    jaccard = len(start_neighbors & end_neighbors)/float(len(start_neighbors | end_neighbors))
                self.training[start_node][end_node]['jaccard'] = jaccard
        for start_node in self.test:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.test[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))

                if len(start_neighbors | end_neighbors) is 0:
                    jaccard = 0
                else:
                    jaccard = len(start_neighbors & end_neighbors)/float(len(start_neighbors | end_neighbors))
                self.test[start_node][end_node]['jaccard'] = jaccard


    def adamic_adar(self):
        
        for start_node in self.training:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.training[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
        
                adamic = 0
                
                for common_neighbor in start_neighbors & end_neighbors:
                    adamic = adamic + 1/math.log(len(self.graphs.unweighted_graph.neighbors(common_neighbor)))

                self.training[start_node][end_node]['adamic'] = adamic
        for start_node in self.test:
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for end_node in self.test[start_node]:
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
        
                adamic = 0
                
                for common_neighbor in start_neighbors & end_neighbors:
                    adamic = adamic + 1/math.log(len(self.graphs.unweighted_graph.neighbors(common_neighbor)))

                self.test[start_node][end_node]['adamic'] = adamic


    def add_common_2_neighbors(self):
        
        for start_node in self.training:
            start_2_neighbors = set()
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for neigh in start_neighbors:
                start_2_neighbors = start_2_neighbors | set(self.graphs.unweighted_graph.neighbors(neigh))
            for end_node in self.training[start_node]:
                end_2_neighbors = set()
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
                for end_neigh in end_neighbors:
                    end_2_neighbors = end_2_neighbors | set(self.graphs.unweighted_graph.neighbors(end_neigh))

                common = len(start_2_neighbors & end_2_neighbors)
                self.training[start_node][end_node]['common2'] = common
        for start_node in self.test:
            start_2_neighbors = set()
            start_neighbors = set(self.graphs.unweighted_graph.neighbors(start_node))
            for neigh in start_neighbors:
                start_2_neighbors = start_2_neighbors | set(self.graphs.unweighted_graph.neighbors(neigh))
            for end_node in self.test[start_node]:
                end_2_neighbors = set()
                end_neighbors = set(self.graphs.unweighted_graph.neighbors(end_node))
                for end_neigh in end_neighbors:
                    end_2_neighbors = end_2_neighbors | set(self.graphs.unweighted_graph.neighbors(end_neigh))

                common = len(start_2_neighbors & end_2_neighbors)
                self.test[start_node][end_node]['common2'] = common
    
                
    def unweighted_katz(self):

        adjacency_matrix = matrix(self.graphs.get_adjacency_matrix())

        ident = [[0 for i in range(len(adjacency_matrix))] for j in range(len(adjacency_matrix))]
        for i in range(len(adjacency_matrix)):
            ident[i][i] = 1
        identity = matrix(ident)

        katz = (identity - 0.01*adjacency_matrix).I - identity

        for start_node in self.training:
            for end_node in self.training[start_node]:
                this_katz = katz[start_node-1, end_node-1]
                self.training[start_node][end_node]['katz'] = this_katz
        for start_node in self.test:
            for end_node in self.test[start_node]:
                this_katz = katz[start_node-1, end_node-1]
                self.test[start_node][end_node]['katz'] = this_katz


    def rooted_pagerank(self):
        
        probability = 0.2

        adjacency = matrix(self.graphs.get_adjacency_matrix())
        row_sums = adjacency.sum(axis=1)
        norm_adj = numpy.zeros(numpy.shape(adjacency))
        for i , (row, rowsum) in enumerate(zip(adjacency, row_sums)):
            if row_sums is 0:
                norm_adj[i,:] = 0
            else:
                norm_adj[i,:] = row / float(rowsum)

        identity = matrix(numpy.identity(numpy.shape(norm_adj)[0]))

        degree = [[0 for i in range(len(norm_adj))] for j in range(len(norm_adj))]
        for i, node in enumerate(self.all_data):
            neighbors = len(self.graphs.unweighted_graph.neighbors(node))
            if neighbors is 0:
                degree[i][i] = sys.maxint
            else:
                degree[i][i] = 1/float(neighbors)
        degree_mat = matrix(degree)

        RPR = (1-probability)*((identity-probability*(degree*norm_adj)).I)
        
        for start_node in self.all_data:
            for end_node in self.all_data[start_node]:
                self.all_data[start_node][end_node]['rpr'] = RPR[start_node-1,end_node-1]



    def drop_column(self, column):
        self.cursor.execute("ALTER TABLE featureset DROP COLUMN " + column)
